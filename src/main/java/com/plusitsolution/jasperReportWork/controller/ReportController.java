package com.plusitsolution.jasperReportWork.controller;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.plusitsolution.jasperReportWork.domain.UploadFilesDomain;
import com.plusitsolution.jasperReportWork.service.ReportService;

@RestController
public class ReportController {

	@Autowired
	private ReportService service;

	
	@PostMapping(path = "/convertExcelToExcel", consumes =  MediaType.MULTIPART_FORM_DATA_VALUE )
    public  void convertExcelToExcel(@RequestParam("uploadFile") MultipartFile uploadfile,@RequestParam(required=true,value="yyyy-MM-dd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws Exception {
    	
    	service.convertExcelToExcel(uploadfile, date);
    }

	@PostMapping(path = "/convertExcelToPDF", consumes =  MediaType.MULTIPART_FORM_DATA_VALUE )
    public  void convertExcelToPDF(@ModelAttribute UploadFilesDomain domain) throws Exception {
 
    	service.convertExcelToPDF(domain);
    }
}

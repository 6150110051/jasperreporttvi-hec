package com.plusitsolution.jasperReportWork.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.plusitsolution.common.toolkit.PlusExcelUtils;
import com.plusitsolution.common.toolkit.PlusFileUtils;
import com.plusitsolution.jasperReportWork.domain.UploadFilesDomain;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Service
public class ReportService {

	public void convertExcelToPDF(UploadFilesDomain domain) throws Exception {
		HashMap<String, Object> map = new HashMap<>();
		for (int i = 0; i < domain.getFiles().size(); i++) {
			File fileXmls = new File(domain.getFiles().get(i).getOriginalFilename());
			fileXmls.createNewFile();
			FileOutputStream fos = new FileOutputStream(fileXmls);
			fos.write(domain.getFiles().get(i).getBytes());
			fos.close();
			if (fileXmls.toString().toLowerCase().endsWith(".xlsx") == true) {
				try {
					Workbook workbook = PlusExcelUtils.initWorkbook(fileXmls);
					Sheet sheetPaySlip = workbook.getSheet("PaySlip");
					
					
					var logo = ClassLoader.getSystemResource("images/logo.png");
					map.put("logo", Files.readAllBytes(Path.of(logo.toURI())));

					var signature = ClassLoader.getSystemResource("images/signature.jpg");
					map.put("signature", Files.readAllBytes(Path.of(signature.toURI())));

					map.put("companyAddress", PlusExcelUtils.readCellAsString(sheetPaySlip, "L3")); // Company Address
					map.put("taxIDNumber", PlusExcelUtils.readCellAsString(sheetPaySlip, "A7")); // Tax ID Number
					map.put("date", PlusExcelUtils.readCellAsString(sheetPaySlip, "C8")); // Date
					map.put("employeeName", PlusExcelUtils.readCellAsString(sheetPaySlip, "C9")); // Employee Name
					map.put("employeeCode", PlusExcelUtils.readCellAsString(sheetPaySlip, "C10")); // Employee Code
					// -- Income --
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F13") == 0.0) {// Salary
						map.put("salary", "-");
					} else {
						map.put("salary", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F13")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F14") == 0.0) {// Medical Fee
						map.put("medicalFee", "-");
					} else {
						map.put("medicalFee",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F14")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F15") == 0.0) {// OT
						map.put("OT", "-");
					} else {
						map.put("OT", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F15")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "D15") == 0.0) { // Date OT
						map.put("dateOT", "-");
					} else {
						map.put("dateOT", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "D15")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F18") == 0.0) {// Income
						map.put("income", "-");
					} else {
						map.put("income", String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F18")));
					}
					// -- Income --
					// -- Deduction --
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L13") == 0.0) {// Income Tax
						map.put("incomeTax", "-");
					} else {
						map.put("incomeTax",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L13")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L14") == 0.0) { // Social Security
						map.put("socialSecurity", "-");
					} else {
						map.put("socialSecurity",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L14")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L15") == 0.0) { // Deduct For Leave
						map.put("deductForLeave", "-");
					} else {
						map.put("deductForLeave",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L15")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "J15") == 0.0) { // Date Deduct For Leave
						map.put("dateDeductForLeave", "-");
					} else {
						map.put("dateDeductForLeave",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "J15")));
					}
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L18") == 0.0) { // Deduction
						map.put("deduction", "-");
					} else {
						map.put("deduction",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "L18")));
					}
					// -- Deduction --
					if (PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F19") == 0.0) { // Net Income
						map.put("netIncome", "-");
					} else {
						map.put("netIncome",
								String.format("%,.2f", PlusExcelUtils.readCellAsNumber(sheetPaySlip, "F19")));
					}
					map.put("approver", PlusExcelUtils.readCellAsString(sheetPaySlip, "I23")); // Approver

					JasperReport compileReport = JasperCompileManager
							.compileReport("src/main/resources/report/report.jrxml");
					JRDataSource datasource = new JREmptyDataSource();
					JasperPrint report = JasperFillManager.fillReport(compileReport, map, datasource);
					byte[] data = JasperExportManager.exportReportToPdf(report);

					Date dateFolder = new SimpleDateFormat("dd-MMM-yy")
							.parse(PlusExcelUtils.readCellAsString(sheetPaySlip, "C8"));
					String newFolder = new SimpleDateFormat("yyyy_MM").format(dateFolder) + "_PDF";

					String[] subNamesEmployee = (String[]) ((String) map.get("employeeCode")).split("-");
					String[] subNamesDate = (String[]) ((String) map.get("date")).split("-");

					String newFile = subNamesEmployee[0] + subNamesEmployee[1] + "_" + subNamesDate[1].toUpperCase()
							+ "_" + subNamesDate[2];
					PlusFileUtils.writeFile(newFolder + "/" + newFile + ".pdf", data);

					workbook.close();
					Path path
		            = Paths.get(fileXmls.getAbsolutePath());
				 Files.deleteIfExists(path);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} finally {
					 if (fos != null) {
							
							fos.close();
						}
						fos = null;	
				}
			}
		}
	}

	public void convertExcelToExcel(MultipartFile uploadfile, Date date) throws Exception {
		File fileXmls = new File(uploadfile.getOriginalFilename());
		fileXmls.createNewFile();
		FileOutputStream fos = new FileOutputStream(fileXmls);
		fos.write(uploadfile.getBytes());

		if (fileXmls.isFile() && fileXmls.toString().toLowerCase().endsWith(".xlsx")) {
			try {
				Workbook wb = PlusExcelUtils.initWorkbook(fileXmls);
				Sheet sheet = wb.getSheet("Salary");

				for (int r = 2; r < sheet.getLastRowNum() - 2; r++) {

					if (sheet.getRow(r).getCell(2).getCellType().equals(org.apache.poi.ss.usermodel.CellType.NUMERIC)) {

						// employeeCode
						String ID = sheet.getRow(r).getCell(0).getStringCellValue();
						String subID1 = ID.substring(0, 4);
						String subID2 = ID.substring(4);
						String employeeCode = subID1 + "-" + subID2;

						// newFile
						SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
						String dateValue = dateFormat.format(date);
						String[] subNamesDate = (String[]) dateValue.split("-");
						String newFile = ID + "_" + subNamesDate[1].toUpperCase() + "_" + subNamesDate[2];

						// newFile
						SimpleDateFormat formatterDate = new SimpleDateFormat("dd-MM-yyyy");
						String strDate = formatterDate.format(date);
						String[] subNamesDate1 = (String[]) strDate.split("-");
						String newFolder = subNamesDate1[2] + "_" + subNamesDate1[1];

						PlusFileUtils.copyFile(new File("src/main/resources/Template.xlsx"),
								newFolder + "/" + newFile + ".xlsx");
						Workbook workbook = PlusExcelUtils.initWorkbook(newFolder + "/" + newFile + ".xlsx");
						Sheet sheet1 = workbook.getSheet("PaySlip");

						PlusExcelUtils.writeCellAsString(sheet1, "C8", dateValue); // Date
						PlusExcelUtils.writeCellAsString(sheet1, "C9", sheet.getRow(r).getCell(1).getStringCellValue()); // Employee
																															// Name
						PlusExcelUtils.writeCellAsString(sheet1, "C10", employeeCode); // Employee Code
						PlusExcelUtils.writeCellAsNumber(sheet1, "F13",
								sheet.getRow(r).getCell(2).getNumericCellValue()); // Salary
						PlusExcelUtils.writeCellAsNumber(sheet1, "F14",
								sheet.getRow(r).getCell(8).getNumericCellValue()); // Medical Fee

						if (PlusExcelUtils.readCellAsNumber(sheet.getRow(r).getCell(10)) != 0.0) { // Other Income With
																									// Tax
							PlusExcelUtils.writeCellAsString(sheet1, "B16", "Other Income With Tax");
							PlusExcelUtils.writeCellAsNumber(sheet1, "F16",
									sheet.getRow(r).getCell(10).getNumericCellValue());
						}
						if (PlusExcelUtils.readCellAsNumber(sheet.getRow(r).getCell(12)) != 0.0) { // Other Income
																									// without Tax
							PlusExcelUtils.writeCellAsString(sheet1, "B17", "Other Income without Tax");
							PlusExcelUtils.writeCellAsNumber(sheet1, "F17",
									sheet.getRow(r).getCell(12).getNumericCellValue());
						}
						PlusExcelUtils.writeCellAsNumber(sheet1, "L13",
								sheet.getRow(r).getCell(18).getNumericCellValue()); // Income Tax
						PlusExcelUtils.writeCellAsNumber(sheet1, "L14",
								sheet.getRow(r).getCell(19).getNumericCellValue()); // Social Security
						if (PlusExcelUtils.readCellAsNumber(sheet.getRow(r).getCell(11)) != 0.0) { // Other Deduct
							PlusExcelUtils.writeCellAsString(sheet1, "H16", "Other Deduct");
							PlusExcelUtils.writeCellAsNumber(sheet1, "L16",
									sheet.getRow(r).getCell(11).getNumericCellValue());
						}
						PlusExcelUtils.writeWorkbook(workbook, newFolder + "/" + newFile + ".xlsx");
					}
				}
				wb.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}finally {
				 if (fos != null) {
						
						fos.close();
					}
					fos = null;	
			}
		}
		Path path
        = Paths.get(fileXmls.getAbsolutePath());
	 Files.deleteIfExists(path);
	}
}